/*
 * ListPeople.cpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#include "ListPeople.h"
#include <iostream>

ListPeople::ListPeople() {
	head = 0;
	last = 0;
	elementsNumber = 0;
}

ListPeople::~ListPeople() {
	// TODO Auto-generated destructor stub
}
Person* ListPeople::setAtElement(int elementNumber) {
	Person *temp = head;
	if (elementNumber >= 0 && elementNumber < elementsNumber)
		for (int i = 0; i < elementNumber; ++i)
			temp = temp->next;
	else
		temp = 0;
	return temp;
}
void ListPeople::printList() {
	Person *temp = head;
	int counter(0);
	while (temp) {
		std::cout << counter << ".\tFirst name: " << temp->firstName
				<< "\tLast name: " << temp->lastName << "\tAge: " << temp->age
				<< std::endl;
		temp = temp->next;
		++counter;
	}
}
void ListPeople::addElementAtEnd(Person *newPerson) {
	if (!elementsNumber) {
		head = newPerson;
		last = newPerson;
	} else {
		Person* temp = head;
		while (temp->next)
			temp = temp->next;
		temp->next = newPerson;
		newPerson->prev = temp;
		last = newPerson;
	}
	++elementsNumber;
}
void ListPeople::addElementAtBegin(Person *newPerson) {
	if (!elementsNumber) {
		head = newPerson;
		last = newPerson;
	} else {
		newPerson->next = head;
		head->prev = newPerson;
		head = newPerson;
	}
	++elementsNumber;
}
void ListPeople::addBeforeElemenet(Person *existingPerson, Person *newPerson) {
	if (existingPerson == head)
		addElementAtBegin(newPerson);
	else {
		newPerson->next = existingPerson;
		newPerson->prev = existingPerson->prev;
		++elementsNumber;
		existingPerson->prev->next = newPerson;
		existingPerson->prev = newPerson;
	}
}
void ListPeople::addAfterElemenet(Person *existingPerson, Person *newPerson) {
	if (existingPerson == last)
		addElementAtEnd(newPerson);
	else {
		newPerson->next = existingPerson->next;
		newPerson->prev = existingPerson;
		++elementsNumber;
		existingPerson->next->prev = newPerson;
		existingPerson->next = newPerson;
	}
}
void ListPeople::deleteLastElement() {
	if (elementsNumber) {
		last = last->prev;
		delete (last->next);
		last->next = 0;
		--elementsNumber;
	}
}
void ListPeople::deleteFirstElement() {
	if (elementsNumber) {
		head = head->next;
		delete head->prev;
		head->prev = 0;
		;
		--elementsNumber;
	}
}
void ListPeople::deleteElemenet (Person *person){
		if(person == head)
			deleteFirstElement();
		else if(person == last)
			deleteLastElement();
		else{
			person->prev->next = person->next;
			person->next->prev = person->prev;
			--elementsNumber;
			delete person;
		}
	}
