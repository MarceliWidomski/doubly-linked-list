/*
 * ListPeople.h
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#ifndef LISTPEOPLE_H_
#define LISTPEOPLE_H_

#include "Person.h"

struct ListPeople {
	ListPeople();
	virtual ~ListPeople();

	Person *head;
	Person *last;
	int elementsNumber;

	Person* setAtElement(int elementNumber);
	void printList ();
	void addElementAtEnd(Person *newPerson);
	void addElementAtBegin(Person *newPerson);
	void addBeforeElemenet (Person *existingPerson, Person *newPerson);
	void addAfterElemenet(Person *existingPerson, Person *newPerson);
	void deleteFirstElement();
	void deleteLastElement();
	void deleteElemenet (Person *existingPerson);
	void deleteAfterElemenet(Person *existingPerson);
};

#endif /* LISTPEOPLE_H_ */
