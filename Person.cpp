/*
 * Person.cpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#include "Person.h"

Person::Person() {
	firstName = "";
	lastName = "";
	age = -1;
	next = 0;
	prev = 0;
}
Person::Person(std::string firstName, std::string lastName, int age) {
	this->firstName = firstName;
	this->lastName = lastName;
	this->age = age;
	this->next = 0;
	this->prev = 0;
}
Person::~Person() {
}

