/*
 * Person.h
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#ifndef PERSON_H_
#define PERSON_H_

#include <string>

struct Person {
	Person();
	Person(std::string firstName, std::string lastName, int age);
	virtual ~Person();
	std::string firstName;
	std::string lastName;
	int age;
	Person *next;
	Person *prev;
};

#endif /* PERSON_H_ */
