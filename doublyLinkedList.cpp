//============================================================================
// Name        : doublyLinkedList.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "ListPeople.h"
#include "Person.h"

int main() {

	ListPeople *listPeople = new ListPeople;

	Person *newPerson = new Person("Rafael", "Nadal", 32);
	listPeople->addElementAtEnd(newPerson);

	newPerson = new Person ("Roger", "Federer", 35);
	listPeople->addElementAtBegin(newPerson);

	Person *existingPerson = listPeople->setAtElement(1);
	newPerson = new Person("Novak", "Djokovic", 30);
	listPeople->addBeforeElemenet(existingPerson, newPerson);

	Person *novak = listPeople->setAtElement(1);
	newPerson = new Person ("Andy", "Murray", 30);
	listPeople->addAfterElemenet(novak, newPerson);
	listPeople->printList();

	listPeople->deleteElemenet(novak);
	novak = 0;
	std::cout << std::endl;
	listPeople->printList();

	return 0;
}
